package golf;

public class Hole {
	
	private boolean holeStatus=true; // true for a free hole, false for occupied
		
	public boolean getHoleStatus() {
		return holeStatus;
	}
	public void setHoleStatus(boolean holeStatus) {
		this.holeStatus = holeStatus;
	}
	
}
