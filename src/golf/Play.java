package golf;

public class Play {
	
	private boolean done=true;
	private boolean busy=false;
	private Hole hole;
	private Thread thread;
	
	public Play(){
		hole= new Hole();
		thread = new Thread(()->changeStatus());
		thread.setDaemon(true);
		thread.start();
}

	public boolean getStatus(){
		// Is synchronization needed??? Is wait() needed??? Is some flag switching needed??? Is notifyAll needed???
		//synchronized (hole){
//			while(busy){
//				try {
//					hole.wait();
//				} catch (InterruptedException e){}
//			}
			return hole.getHoleStatus();
		//}
	};	
	
	public void lockHole(){
		synchronized (hole){
			while(busy){
				try {
					hole.wait();
				} catch (InterruptedException e){}
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			hole.setHoleStatus(false);
			done=false;
			busy=true;
			hole.notifyAll();
		};
	}
		
	public void hitHole(){
		synchronized (hole){
			while(done = false){
				try {
					hole.wait();
				} catch (InterruptedException e){}
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			hole.setHoleStatus(true);
			busy=false;
			hole.notifyAll();
		}
	}
	
	private  void changeStatus(){
		synchronized (hole){
			while (true){
				while(done){
					try {
						hole.wait();
				}catch (InterruptedException e){}
			}
			done=true;
			hole.notifyAll();
		}
	}
	}
}
