package golf;

import java.util.Random;

public class Golf_App {
	
	private static int numberHoles=3; 
	private static Random rn = new Random();
	private static int maxTimeHoleHit=1000; // Used to generate random number in msec between lock and hit events 
	private static int walkingTime=500; // sleep time between hit hole N and start waiting for hole N+1
	private static int intervalCheckHole=100; // time interval for checking (by the player who locked the whole) if hole is free
	private static Play play;
	
	public static void main(String [] args){
		
		play= new Play();
		
		Thread thread0= new Thread(() -> playHoles());
		Thread thread1= new Thread(() -> playHoles());		
		Thread thread2= new Thread(() -> playHoles());	
		Thread thread3= new Thread(() -> playHoles());
		Thread thread4= new Thread(() -> playHoles());		
		Thread thread5= new Thread(() -> playHoles());			
		thread0.setName("Player 0");
		thread1.setName("Player 1");
		thread2.setName("Player 2");	
		thread3.setName("Player 3");
		thread4.setName("Player 4");
		thread5.setName("Player 5");			
		thread0.start();
		thread1.start();
		thread2.start();			
		thread3.start();
		thread4.start();
		thread5.start();		
	}
	
	public static void playHoles(){
		for (int i=0; i< numberHoles; i++){
			System.out.printf("%s waits for hole %d\n", Thread.currentThread().getName(), i);
				while (play.getStatus()==false){ // keep waiting till the whole is free				
				try {
					Thread.sleep(intervalCheckHole);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			};
			play.lockHole(); // the player locks the hole (sets hole status to false- to prevent other players accessing it)		
			System.out.printf("%s locks hole %d\n", Thread.currentThread().getName(), i);
			int hitTime= rn.nextInt(maxTimeHoleHit); 
			try {
				Thread.sleep(hitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} // try hitting ball this long
			play.hitHole(); // ball falls in the hole
			System.out.printf("%s hits hole %d and leaves for another one\n", Thread.currentThread().getName(), i);
			try {
				Thread.sleep(walkingTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
